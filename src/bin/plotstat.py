#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   This file belong to https://github.com/snolfi/evorobotpy
   and has been written by Stefano Nolfi, stefano.nolfi@istc.cnr.it

   plotstat.py the fitness across generation contatined in stat*.npy files

"""


import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys
import os

print("plotstat.py")
print("plot the fitness across generation contatined in stat*.npy files")
print("if called with a filename, plot the data of that file")
print("if called without parameters, plot the data of all available stat*.npy files")
print("")

# plot the data contained in the parameter file
# if called without parameters plot all available statS?.npy files

statsumn = 0
statavesum = 0
np.random.seed(1)

if len(sys.argv) == 1:
    sns.set_theme()
    cpath = os.getcwd()
    files = os.listdir(cpath)
    # plt.title("statS*.npy")
    print("Plotting data contained in:")
    minl = 999999999
    maxl = -minl
    dfarr = []
    for f in files:
        if "statS" in f:
            print(f)
            stat = np.load(f, allow_pickle=True)
            size = np.shape(stat)
            newsize = (int(size[0] / 6), 6)
            stat = np.resize(stat, newsize)
            if len(stat[:]) < minl:
                minl = len(stat[:])
            if len(stat[:]) > maxl:
                maxl = len(stat[:])
            if statsumn == 0:
                statsum = np.zeros((6,6))
                ss = pd.DataFrame(statsum)
                ss.rename(columns={0 : "steps", 2 : "gfit"}, inplace=True)
            df = pd.DataFrame(stat)
            df.rename(columns={0 : "steps", 2 : "gfit"}, inplace=True)
            dfarr.append(df)
            statavesum += 1
            statsumn = statsumn + 1
            
    get_value = lambda df, i : df.iloc[[i]]["gfit"].item()

    def dfaverage(dfarr, i):
        s = 0
        for df in dfarr:
            try:
                value = get_value(df, i)
                s += value
            except:
                continue
        return s/len(dfarr)
        
    def dfmin(dfarr):
        m = float("inf")
        for df in dfarr:
            if m > len(df):
                m = len(df)
                smalldf = df
        return [m, smalldf]
        
    def getsteps(df, i):
        steps = get_steps(dfmin(dfarr)[1], i)
        return steps
            
    dfunion_average = [dfaverage(dfarr, i) for i in range(dfmin(dfarr)[0])]
    dfunion = pd.DataFrame({"gfit" : dfunion_average, "gens": [i for i in range(len(dfunion_average))]})
    dfminlen = [i for i in range(dfmin(dfarr)[0])]
    #for df in dfarr:
    #    sns.lineplot(
    #        x=dfminlen, y=[get_value(df, i) for i in range(len(dfminlen))],
    #    )
    
    sns.lineplot(
        data=dfunion,
        x="gens", y="gfit",
    ).set(title=input("Insert graph title: "))
    if statsumn == 0:
        print("\033[1mERROR: No stat*.npy file found\033[0m")
    else:
        plt.legend()
        plt.show()


else:
    if len(sys.argv) == 2:
        stat = np.load(sys.argv[1])
        size = np.shape(stat)
        newsize = (int(size[0] / 6), 6)
        stat = np.resize(stat, newsize)
        stat = np.transpose(stat)
        plt.title(sys.argv[1])
        plt.plot(stat[0], stat[1], label="fit", linewidth=1, color="r")
        plt.plot(stat[0], stat[2], label="gfit", linewidth=1, color="b")
        plt.legend()
        plt.show()
