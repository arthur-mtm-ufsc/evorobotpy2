#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   This file belong to https:/github.com/arthurholtrup/evorobotpy2
   and has been written by Arthur H. Bianchini, arthur.h.bianchini@grad.ufsc.br
"""


import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys
import os

print("plotphylo.py")
print("plot the phylogenetic contatined in phylo*.npy files")
print("")


sns.set_theme()
cpath = os.getcwd()
files = os.listdir(cpath)
print("Plotting data contained in:")
for f in files:
    if "phylo" in f:
        print(f)
        prop_cycle = plt.rcParams['axes.prop_cycle']
        colors = prop_cycle.by_key()['color']
        phyloy = np.load(f, allow_pickle=True)
        numberlists = len(phyloy)
        numberniches = len(colors)
        lenniche = len(phyloy[0])
        for lista in range(numberlists):
            plt.plot(range(0, lenniche*50, 50), phyloy[lista], color=colors[lista%numberniches])
        plt.xlabel("Generation")
        plt.ylabel("Niche")
        plt.title(input("Insert plot title: "))
        plt.yticks(range(1, numberniches+1))
        plt.legend()
        plt.show()
            