#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
   This file belong to https://github.com/snolfi/evorobotpy
   and has been written by Stefano Nolfi, stefano.nolfi@istc.cnr.it

   plotstat.py the fitness across generation contatined in stat*.npy files

"""


import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys
import os

print("plotcompare.py")
print("plot the fitness across generation contatined in stat*.npy files")
print("if called with a filename, plot the data of that file")
print("if called without parameters, plot the data of all available stat*.npy files")
print("")

# plot the data contained in the parameter file
# if called without parameters plot all available statS?.npy files

statsumn = 0
statavesum = 0
np.random.seed(1)

if len(sys.argv) == 1:
    sns.set_theme()
    cpath = os.getcwd()
    files = os.listdir(cpath)
    # plt.title("statS*.npy")
    print("Plotting data contained in:")
    minl = 999999999
    maxl = -minl
    dfarres = []
    dfarresne = []
    dfarrsss = []
    dfarrne = []
    for f in files:
        if "statS" in f:
            print(f)
            fint = f.replace("statS", '')
            fint = fint.replace(".npy", '')
            stat = np.load(f, allow_pickle=True)
            size = np.shape(stat)
            newsize = (int(size[0] / 6), 6)
            if 20 < int(fint) <= 30:
                newsize = (int(size[0]) // 4, 4)
            stat = np.resize(stat, newsize)
            
            if len(stat[:]) < minl:
                minl = len(stat[:])
            if len(stat[:]) > maxl:
                maxl = len(stat[:])
            if statsumn == 0:
                statsum = np.zeros((6,6))
                ss = pd.DataFrame(statsum)
                ss.rename(columns={0 : "steps", 2 : "gfit"}, inplace=True)
            df = pd.DataFrame(stat)
            df.rename(columns={0 : "steps", 2 : "gfit"}, inplace=True)
            if int(fint) <= 10:
                dfarresne.append(df)
            if 10 < int(fint) <= 20:
                dfarres.append(df)
            if 20 < int(fint) <= 30:
                dfarrne.append(df)
            else:
                dfarrsss.append(df)    
            statavesum += 1
            statsumn = statsumn + 1
            
    get_value = lambda df, i : df.iloc[[i]]["gfit"].item()
        
    def dfmin(dfarr):
        m = float("inf")
        for df in dfarr:
            if m > len(df):
                m = len(df)
                smalldf = df
        return [m, smalldf]
        
    def getsteps(df, i):
        steps = get_steps(dfmin(dfarr)[1], i)
        return steps
    
    def plotave(dfarr):
        dfmini = dfmin(dfarresne+dfarres+dfarrsss+dfarrne)[0]
        dfminlen = [i for i in range(dfmini)] 
        gggeeennnsss = [i for i in range(len(dfminlen)) for _ in range(len(dfarr))]
        dfminlen = [i for i in range(dfmini)]
        dfunion = pd.DataFrame({"Generation" : gggeeennnsss,
            "seed" : [i%len(dfarr) for i in range(len(dfminlen)*len(dfarr))],
            "Generalized fitness" : [get_value(dfarr[i], j) for j in range(len(dfminlen)) for i in range(len(dfarr))]})

        sns.lineplot(
            data=dfunion,
            x="Generation", y="Generalized fitness",
            ci = 68,
        )
          
    plotave(dfarresne)
    plotave(dfarres)
    plotave(dfarrne)
    plotave(dfarrsss)
    if statsumn == 0:
        print("\033[1mERROR: No stat*.npy file found\033[0m")
    else:
        plt.title(input("Insert plot title: "))
        plt.legend(labels=["ES-NE Average", "ES-NE Confidence", "ES Average", "ES Confidence", "NE Average", "NE Confidence", "SSS Average", "SSS Confidence"])
        plt.show()


else:
    if len(sys.argv) == 2:
        stat = np.load(sys.argv[1])
        size = np.shape(stat)
        newsize = (int(size[0] / 6), 6)
        stat = np.resize(stat, newsize)
        stat = np.transpose(stat)
        plt.title(sys.argv[1])
        plt.plot(stat[0], stat[1], label="fit", linewidth=1, color="r")
        plt.plot(stat[0], stat[2], label="gfit", linewidth=1, color="b")
        plt.legend()
        plt.show()
