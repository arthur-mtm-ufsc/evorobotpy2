from stats.context_stats import ContextStats

seeds = [1,2]

stats = ContextStats('data/xdpole', seeds)

stats.fitness_evolution()
stats.fitness_evolution_boxplot()
